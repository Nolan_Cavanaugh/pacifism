﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSpawner : MonoBehaviour
{
    public static ExplosionSpawner instance;

    public GameObject explosionPrefab;

    private static List<GameObject> explosion;
    private AudioManager audioMan;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            explosion = new List<GameObject>();
        }
    }

    public static void Spawn(Vector3 pos)
    {
        GameObject g = explosion.Find(IsInactive);
        if (g == null)
        {
            g = Instantiate(instance.explosionPrefab);
            explosion.Add(g);
            AudioManager.Play("spawnExplosion");
            ParticleManager.Play("ExplosionSpawner", pos);
        }
        g.transform.position = pos;
        g.SetActive(true);
    }

    static bool IsInactive(GameObject g)
    {
        return !g.activeSelf;
    }
}
