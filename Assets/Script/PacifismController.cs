﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PacifismController : MonoBehaviour
{
    public static PacifismController instance;

    public GateController gate;
    public EnemyController enemy;
    public PlayerScript player;

    public GameObject explosion;
    public GameObject pointAdd;

    static int score = 0;
    static int highScore = 0;
    static int multiplier = 1;

    public Text scoreText;
    public Text highScoreText;
    public Text multiplierText;
    public Text gameOver;

    void Awake()
    {
        
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        gameOver.enabled = false;

        highScore = PlayerPrefs.GetInt("HighScore", 0);
        scoreText.text = "0";
        highScoreText.text = highScore.ToString();
        multiplierText.text = "x1";
       
    }

    public static void ScorePoints (int points)
    {
        score += points * multiplier;
        instance.scoreText.text = score.ToString();

        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("HighScore", highScore);
            instance.highScoreText.text = highScore.ToString();
        }

    }

    public static void AddToMultiplier()
    {
        Debug.Log("Multipler works");
        multiplier++;
        instance.multiplierText.text = "x" + multiplier;
    }

    public static void PlayerDied()
    {
        Debug.Log("Die");
        instance.StartCoroutine(instance.GameOverCoroutine());
    }

    IEnumerator GameOverCoroutine ()
    {
        Debug.Log("Die2");
        gameOver.gameObject.SetActive(true);
        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene("Menu");
    }
}
