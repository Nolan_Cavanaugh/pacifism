﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript player;
    public float speed = 5;
    public Text gameOver;
    public bool gameMode = true;
    public Vector3 pos;

    public GameObject bulletPrefab;
    GameObject[] bullets;

    public float roundsPerSecond = 30;
    public int maxBullets = 30;
    bool bReloading = false;
    public bool bPacifism = true;

    AudioManager audioMan;
    Rigidbody body;

    private void Awake()
    {
        if (player == null)
        {
            player = this;
        }
    }

    void Start()
    {
        body = GetComponent<Rigidbody>();
        audioMan = FindObjectOfType<AudioManager>();
        bullets = new GameObject[maxBullets];
        for (int i = 0; i < maxBullets; i++)
        {
            GameObject g = Instantiate(bulletPrefab);
            g.SetActive(false);
            bullets[i] = g;
        }
    }

    
    void Update()
    {
        float x = Input.GetAxis("MoveHorizontal");
        float z = Input.GetAxis("MoveVertical");
        Vector3 heading = new Vector3(x, 0, z);

        float d = heading.sqrMagnitude;
        if (d > 1) heading.Normalize();
        if (d > .1f) transform.forward = heading;

        body.velocity = heading * speed;

        x = Input.GetAxis("ShootHorizontal");
        z = Input.GetAxis("ShootVertical");
        bool fire = Input.GetKeyDown(KeyCode.Space);
        Vector3 facing = new Vector3(x, 0, z);

        d = facing.sqrMagnitude;
        if (d > 1) facing.Normalize();
        if (d > .5f) transform.forward = facing;

        if (fire == true && !bReloading && !bPacifism)
        {
            Debug.Log("Fires");
            StartCoroutine(ShootBullet());
        }


        bool reset = Input.GetKeyDown(KeyCode.Escape);

        if (reset == true)
        {
            SceneManager.LoadScene("Menu");
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<EnemyController>())
        {
            player.die();
        }
        if (other.gameObject.GetComponent<EnemyDosController>())
        {
            player.die();
        }
        if (other.gameObject.GetComponent<EnemyTresController>())
        {
            player.die();
        }
        if (other.gameObject.GetComponent<GateController>())
        {
            player.die();       
        }
    }

    IEnumerator ShootBullet()
    {
        bReloading = true;
        for (int i = 0; i < bullets.Length; i++)
        {
            if (!bullets[i].activeSelf)
            {
                bullets[i].SetActive(true);
                bullets[i].GetComponent<BulletController>().Shoot(transform.forward, transform.position);
                break;
            }
        }
        AudioManager.Play("BulletFires");
        yield return new WaitForSeconds(1 / roundsPerSecond);
        bReloading = false;
    }

    public void die()
    {
        AudioManager.Play("Death");
        Vector3 pos = player.transform.position;
        ParticleManager.Play("PlayerDeathParticles", pos);
        player.gameObject.SetActive(false);
        if (gameMode == true)
        {
            PacifismController.PlayerDied();
        }
        else if (gameMode == false)
        {
            NotSoPacifism.PlayerDied();
        }
    }
}
