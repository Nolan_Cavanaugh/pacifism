﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierSpawner : MonoBehaviour
{
    public static MultiplierSpawner instance;

    public GameObject multiplierPrefab;
    public int initialSize = 100;


    private static List<GameObject> multipliers;
    private AudioManager audioMan;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            multipliers = new List<GameObject>();
        }
    }

    void Start()
    {
        audioMan = FindObjectOfType<AudioManager>();
        for (int i = 0; i < initialSize; i++)
        {
            GameObject g = Instantiate(multiplierPrefab);
            g.SetActive(false);
            multipliers.Add(g);
        }
    }

    static bool IsInactive(GameObject g)
    {
        return !g.activeSelf;
    }

    public static void Spawn(Vector3 pos, float range, int total)
    {
        for (int i = 0; i < total; i++)
        {
            GameObject g = multipliers.Find(IsInactive);
            if (g ==null)
            {
                g = Instantiate(instance.multiplierPrefab);
                multipliers.Add(g);
            }

            Vector3 p = pos + Random.insideUnitSphere * range;
            p.y = 0;
            g.transform.position = pos;
            g.SetActive(true);
        }
    }

    public static void Cleanup()
    {
        for (int i = multipliers.Count-1; i >= 0; i--)
        {
            multipliers[i].SetActive(false);
        }
    }
}