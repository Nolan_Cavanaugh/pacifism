﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTresController : MonoBehaviour
{
    NotSoPacifism game;
    public static float speed = 3;
    public int points = 5;

    public GameObject ScoreUpdate;
    public GameObject enemy;
    public GameObject multiplier;
    Rigidbody body;

    void Start()
    {
        game = FindObjectOfType<NotSoPacifism>();
        body = GetComponent<Rigidbody>();
    }


    void Update()
    {
        
        Vector3 dir = PlayerScript.player.transform.position - transform.position;
        if (dir.sqrMagnitude > 1)
        {
            dir.Normalize();
        }

        transform.forward = dir;
        body.velocity = dir * speed;

    }


    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.GetComponentInParent<ExplosionController>())
        {
            StartCoroutine(PointValue());
            enemy.SetActive(false);
            points = 5;
            NotSoPacifism.ScorePoints(points);
        }
        if (c.gameObject.GetComponentInParent<ExplosionController>() != null)
        {
            MultiplierSpawner.Spawn(transform.position, 0.1f, 3);
            NotSoPacifism.ScorePoints(points);
            gameObject.SetActive(false);
        }
        if (c.gameObject.GetComponentInParent<BulletController>())
        {
            StartCoroutine(PointValue());
            enemy.SetActive(false);
            points = 6;            
            NotSoPacifism.ScorePoints(points);            
        }
         if (c.gameObject.GetComponentInParent<BulletController>() != null)
        {
            MultiplierSpawner.Spawn(transform.position, 0.1f, 3);
            NotSoPacifism.ScorePoints(points);
            gameObject.SetActive(false);
        }
    }

    IEnumerator PointValue()
    {
        GameObject g = Instantiate(ScoreUpdate);
        g.transform.position = enemy.transform.position;
        yield return new WaitForSeconds(1.5f);
        g.SetActive(false);
        yield return new WaitForEndOfFrame();
    }
}
