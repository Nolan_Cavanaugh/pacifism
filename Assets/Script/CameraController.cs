﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float yOffset = 10;
    [Range(0, 30)] public float maxTilt = 5;
    public Vector3 maxMove = new Vector3(5, 0, 10);
    public GameObject arena;

    private Bounds bounds;

    void Start()
    {
        bounds = arena.GetComponent<Renderer>().bounds;

    }

    void Update()
    {
        Vector3 pos = PlayerScript.player.transform.position;
        float x = pos.x / bounds.extents.x;
        float z = pos.z / bounds.extents.z;
        transform.position = new Vector3(x * maxMove.x, yOffset, z * maxMove.z);
        transform.eulerAngles = new Vector3(x, 0, z) * maxTilt;
    }
}
