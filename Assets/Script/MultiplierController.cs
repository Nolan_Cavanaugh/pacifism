﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierController : MonoBehaviour
{
    public static MultiplierController instance;

    public float lifeSpan = 5; 
    public float driftRange = 3;
    public bool gameMode = true;
    public GameObject prefab;
    Rigidbody body;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        body = GetComponent<Rigidbody>();
    }
    private void OnEnable()
    {
        Vector3 v = Random.insideUnitSphere;
        v.y = 0;
        body.velocity = v;
        StartCoroutine(SelfDestructCoroutine());
    }

    IEnumerator SelfDestructCoroutine()
    {
        yield return new WaitForSeconds(lifeSpan);
        gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider c)
    {
        Debug.Log("Collided");

        if (c.GetComponentInParent<PlayerScript>())
        {
            Debug.Log("Collid2");
            if (gameMode == true)
            {
                PacifismController.AddToMultiplier();
            }
            else if (gameMode == false)
            {
                NotSoPacifism.AddToMultiplier();
            }
            AudioManager.Play("Multiplier");
            prefab.SetActive(false);
        }
        
        
    }

    void Update()
    {
        Vector3 dir = PlayerScript.player.transform.position - transform.position;
        if (dir.sqrMagnitude < driftRange * driftRange)
        {
            body.velocity = dir.normalized * driftRange / dir.sqrMagnitude;
        }
    }
}
