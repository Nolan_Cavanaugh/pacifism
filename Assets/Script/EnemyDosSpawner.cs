﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDosSpawner: MonoBehaviour
{
    public static EnemyDosSpawner instance;

    public GameObject enemyPrefab;
    public int maxEnemies = 15;
    public int initialSize = 10;
    public float interval = 2;
    public GameObject[] spawnPoints;

    public int num;
    private List<GameObject> enemies;
    private AudioManager audioMan;

    void awake()
    {
        if (instance = null)
        {
            instance = this;
        }
    }

    Coroutine spawnCoroutine;
    void Start()
    {
        audioMan = FindObjectOfType<AudioManager>();
        enemies = new List<GameObject>();
        for (int i = 0; i < initialSize; i++)
        {
            GameObject g = Instantiate(enemyPrefab);
            g.SetActive(false);
            enemies.Add(g);
        }

        spawnCoroutine = StartCoroutine(SpawnCoroutine());
    }

    public GameObject Spawn()
    {

        for (int i = 0; i < initialSize; i++)
        {
            if (!enemies[i].activeSelf)
            {
                return enemies[i];
            }
        }
        return null;
    }

    void Spawn(Vector3 pos, float range, int total)
    {
        for(int i = 0; i < total; i++)
        {
            GameObject g = enemies.Find(IsInactive);
            if (g == null && enemies.Count < maxEnemies)
            {
                g = Instantiate(enemyPrefab);
                enemies.Add(g);
            }
            
            if (g != null)
            {
                Vector3 p = pos + Random.insideUnitSphere * range;
                p.y = 0;
                g.transform.position = p;
                g.SetActive(true);
            }       
        }        
    }

    bool IsInactive(GameObject g)
    {
        return !g.activeSelf;
    }

    IEnumerator SpawnCoroutine()
    {
        int num = 1;
        while (enabled)
        {
            Vector3 pos = spawnPoints[0].transform.position;
            float dist = (pos - PlayerScript.player.transform.position).sqrMagnitude;
            for (int i = 1; i < spawnPoints.Length; i++)
            {
                float d = (spawnPoints[i].transform.position - PlayerScript.player.transform.position).sqrMagnitude;
                if (d > dist)
                {
                    dist = d;
                    pos = spawnPoints[i].transform.position;
                }
            }
            ParticleManager.Play("EnemyDosSpawnParticles", pos);
            yield return new WaitForSeconds(.1f);

            Spawn(pos, 2 , num);
            num++;
            AudioManager.Play("EnemyDosSpawn");           
            yield return new WaitForSeconds(interval);
        }
    }

    public static void Cleanup()
    {
        instance.StopCoroutine(instance.spawnCoroutine);
        for (int i = instance.enemies.Count-1; i >= 0; i--)
        {
            instance.enemies[i].SetActive(false);
        }
    }
}
