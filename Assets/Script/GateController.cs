﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour
{
    public Transform[] explosionPoints;

    private void OnEnable()
    {
        Rigidbody body = GetComponent<Rigidbody>();
        Vector3 v = Random.insideUnitSphere;
        v.y = 0;
        body.velocity = v;
        body.angularVelocity = Vector3.up * Random.Range(-1, 1);
    }
    private void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.GetComponent<PlayerScript>() == null)
        {

        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.GetComponentInParent<PlayerScript>() == null)
        {
            MultiplierSpawner.Spawn(transform.position, 1, 1);
        }

        
        if (other.GetComponentInParent<PlayerScript>())
        {
            for (int i = 0; i < explosionPoints.Length; i++)
            {
                AudioManager.Play("spawnExplosion");
                
                ExplosionSpawner.Spawn(gameObject.transform.position);
            }
        }
        gameObject.SetActive(false);
    }
}
