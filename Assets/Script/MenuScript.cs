﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public Animator trasitionAnim;

    public void StartGame()
    {
        StartCoroutine(LoadPac());
    }

    public void StartNotSo()
    {
        StartCoroutine(LoadNotSo());
    }

    public void Stop()
    {
        AudioManager.Play("Selector");
        Application.Quit();
    }

    void Update()
    {
        bool escape = Input.GetKeyDown(KeyCode.Escape);
        if (escape == true )
        {
            Application.Quit();
        }
    }

    IEnumerator LoadPac()
    {
        AudioManager.Play("Selector");
        trasitionAnim.SetBool("Start", true);
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("Pacifism");
    }

    IEnumerator LoadNotSo()
    {
        AudioManager.Play("Selector");
        trasitionAnim.SetBool("Start", true);
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("NotSoPacifism");
    }


}
