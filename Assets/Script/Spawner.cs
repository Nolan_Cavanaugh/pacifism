﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static Spawner spawner;

    public GameObject[] prefabs;
    private Dictionary<string, List<GameObject>> pools = new Dictionary<string, List<GameObject>>();
    private Dictionary<string, GameObject> prefabDict = new Dictionary<string, GameObject>();

    private void Awake()
    {
        if (spawner == null)
        {
            spawner = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogWarning("Spawner Already Exists. Disabling.");
            gameObject.SetActive(false);
        }
    }

    void Start()
    {

        for (int i = 0; i < prefabs.Length; i++)
        {
            string name = prefabs[i].name;

            pools[name] = new List<GameObject>();
            prefabDict[name] = prefabs[i];
        }
    }

    public static GameObject Spawn (string name)
    {
        if (!spawner.pools.ContainsKey(name))
        {
            Debug.LogWarning("no such Particle");
            return null;
        }

        GameObject g = spawner.pools[name].Find(IsInactive);        
        if(g == null)
        {
            g = Instantiate(spawner.prefabDict[name]);
            spawner.pools[name].Add(g);
        }

        return null;
    }

    static bool IsInactive(GameObject g)
    {
        return !g.activeSelf;
    }
}
