﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float speed = 30;
    Rigidbody body;

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
    }

    public void Shoot(Vector3 dir, Vector3 pos)
    {
        transform.position = pos;
        body.velocity = speed * dir;
    }

    public void OnCollisionEnter(Collision c)
    {
        gameObject.SetActive(false);
    }
}
