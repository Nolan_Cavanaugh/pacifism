﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateSpawner : MonoBehaviour
{
    public static GateSpawner instance;

    public GameObject gatePrefab;
    public int maxGates = 10;
    public float interval = 3;

    private GameObject[] gates;

    public GameObject arena;
    private Bounds bounds;

    void awake()
    {
        if (instance = null)
        {
            instance = this;
        }
    }


    Coroutine spawnCoroutine; 
    private void Start()
    {
        bounds = arena.GetComponent<Renderer>().bounds;
        gates = new GameObject[maxGates];
        for (int i = 0; i < maxGates; i++)
        {
            GameObject g = Instantiate(gatePrefab);
            g.SetActive(false);
            gates[i] = g;
        }

        spawnCoroutine = StartCoroutine(SpawnCoroutine());
    }

    public GameObject Spawn()
    {
        for( int i = 0; i < maxGates; i++)
        {
            if (!gates[i].activeSelf)
            {
                return gates[i];
            }
        }
        return null;
    }

    IEnumerator SpawnCoroutine()
    {
        while (enabled)
        {
            Vector3 pos = new Vector3(
                Random.Range(-bounds.extents.x, bounds.extents.x),
                0,
                Random.Range(-bounds.extents.y, bounds.extents.y)
            );
                
            ParticleManager.Play("GateSpawnParticles", pos);
            yield return new WaitForSeconds(1f);

            GameObject g = Spawn();
            if (g != null)
            {
                g.transform.position = pos;
                g.SetActive(true);
                AudioManager.Play("SpawnGate");
            }
            yield return new WaitForSeconds(interval);
        }
    }

    public static void Cleanup()
    {
        instance.StopCoroutine(instance.spawnCoroutine);
        for (int i = 0; i < instance.maxGates; i++)
        {
            instance.gates[i].SetActive(false);
        }
    }
}
