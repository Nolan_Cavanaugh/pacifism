﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public static ParticleManager instance;

    public ParticleSystem[] particlePrfabs;
    public int maxParticles = 20;

    private static Dictionary<string, ParticleSystem[]> systems = new Dictionary<string, ParticleSystem[]>();
    private static Dictionary<string, int> indices = new Dictionary<string, int>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        int count = particlePrfabs.Length;
        for (int i = 0; i < count; i ++)
        {
            string name = particlePrfabs[i].name;
            ParticleSystem[] pool = new ParticleSystem[maxParticles];
            for (int j = 0; j < maxParticles; j++)
            {
                pool[j] = Instantiate(particlePrfabs[i]);
            }
            systems[name] = pool;
            indices[name] = 0;
        }
    }

    public static void Play (string systemName, Vector3 position)
    {
        if (!systems.ContainsKey(systemName))
        {
            Debug.LogWarning("no such Particle");
            return;
        }
        ParticleSystem[] pool = systems[systemName];
        int index = indices[systemName];
        ParticleSystem system = pool[index];
        system.transform.position = position;
        system.Play();
        indices[systemName] = (index + 1) % instance.maxParticles;
    }
}
